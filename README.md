# Peltier Cooling for Test Modules
Peltier cooling system and PID control loop. The peltier_cooling program requires the following parameters:

* Use `-a` or `--port` to specify the address of the port connected to the Arduino (e.g. /dev/ttyACM0).
* Use `-e` or `--config` to specify the configuration file to use for the power supply (e.g. ~/input-hw.json).
* Use `-c` or `--channel` to specify the power supply channel name, as defined in the configuration file, connected to the peltier (e.g. peltier). Alternatively, you can specify the channel number, but then you also need to specify the power supply name.
* Use `-P` or `--powersuppply` to specify the power supply name, as defined in the configuration file (e.g. bk1688b). This is only necessary if the channel is given as a number.

Example: `./peltier_cooling -a /dev/ttyACM0 -e ~/input-hw.json -c peltier`.

In addition, the peltier_cooling program may be called with the following optional parameters:

* Use `-t` or `--temp` to set the target temperature in Celcius (the default is -5 C).
* Use `-p` or `--pin` to set the Arduino pin number connected to the NTC (the default is pin 0).
* Use `-r` or `--resistance` to set the resistance in the voltage divider (the default is 10,000 ohms).
* Use `-d` or `--debug` to enable more verbose printout.
* Use `-h` or `--help` to list the commands and options available.

The sketch *devcommuino.ino*, defined in labRemote, should be uploaded to the Arduino before running this program.
