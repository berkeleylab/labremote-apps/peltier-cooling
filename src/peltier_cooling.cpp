//#include <unistd.h>
//#include <stdlib.h>

#include <iostream>
#include <string>
#include <getopt.h>
#include <sstream>
#include <memory>
#include <cstdint>
#include <chrono>
#include <thread>
#include <cstdint>
#include <csignal>

#include "Logger.h"
#include "TextSerialCom.h"
#include "EquipConf.h"
#include "PowerSupplyChannel.h"
#include "NTCSensor.h"

#include "PID.h"

// Peltier parameters
double voltage;
double kp=3.0;
double ki=0.5;
double kd=19.5;
double set_point=273.15-5;
double max_voltage = 12.5;
// Timeout error may occur if min_voltage is set below 1.0
double min_voltage = 1.0;
double go_time_temp = (max_voltage / kp);
double  duration = 200.0;

std::string config_file;
std::string channelName;
std::string PSname;
int channel=1;

// NTC paramters
unsigned num_ntc = 1;
// Voltage across the NTC voltage divider circuit
float NTCvoltage=5.05;
// The pin on the Arduino connected to the NTC
uint8_t pin=0;
// The address of the device port connected to the Arduino
std::string device=""; 
// Resistance of resistor in voltage divider
float Rvdiv=10000.0;
// NTC specs
// Reference temperature in Kelvin
float refTemp=298.15;
// Resistance at reference temperature in ohms
float Rref=10000.0;
// B value for NTC in Kelvin
float Bntc=3380;

std::shared_ptr<PowerSupplyChannel> PS;

void usage(char *argv[])
{
  std::cerr << "" << std::endl;
  std::cerr << "Usage: " << argv[0] << " [options]" << std::endl;
  std::cerr << "List of options:" << std::endl; 
  std::cerr << " -t, --temp T           Set the target temperature in Celsius (default: " << set_point-273.15 << ")" << std::endl;  
  std::cerr << " -p, --pin P            Set the Arduino pin number connected to the NTC (default: " << pin << ")" << std::endl;  
  std::cerr << " -r, --resistance R     Set the resistance in the voltage divider (default: " << Rvdiv << ")" << std::endl;  
  std::cerr << " -a, --port ADDR        Set the address of the port connected to the Arduino (default: " << device << ")" << std::endl;  
  std::cerr << " -e, --config ADDR      Set theconfiguration file to use for the power supply (default: " << config_file << ")" << std::endl;
  std::cerr << " -c, --channel NAME     Set the name of the power supply channel connected to the peltier" << std::endl;
  std::cerr << " -c, --channel NUMBER   Set the number of the power supply channel connected to the peltier (default: " << channel << ")" << std::endl;
  std::cerr << " -P, --powersupply NAME Set the name of the power supply connected to the peltier (needed if channel number is set instead of channel name)" << std::endl;
  std::cerr << " -d, --debug            Enable more verbose printout, use multiple for increased debug level"  << std::endl;
  std::cerr << " -h, --help             List the commands and options available"  << std::endl;
  std::cerr << "" << std::endl;
}

// Handle keyboard interrupt
void signalHandler( int signum ) {
   std::cout << "Turning off power supply.\n";
   PS->turnOff();
   exit(signum);  
}

int main(int argc, char *argv[])
{
  // Parse command-line
  if (argc < 1)
  {
    usage(argv);
    return 1;
   }
  
  int c;
  while (true) {
    int option_index = 0;
    static struct option long_options[] = {
      {"temp", required_argument, 0, 't' },
      {"pin", required_argument, 0,  'p' },
      {"resistance",  required_argument, 0,  'r' },
      {"port",  required_argument, 0,  'a' },
      {"config", required_argument, 0, 'e'},
      {"channel", required_argument, 0, 'c'},
      {"powersupply", required_argument, 0, 'P'},
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {0,          0,                 0,  0 }
    };
                                                                                
    c = getopt_long(argc, argv, "t:p:r:a:e:c:P:dh", long_options, &option_index);
    if (c == -1)  break;
    switch (c) {
      case 't':
        try
        {
	    set_point = (double)std::stoi(optarg)+273.15;
        }
        catch(const std::invalid_argument& e)
        {
	    std::cerr << "Temperature must be a number. "<< optarg << "supplied. Aborting." << std::endl;
      	    std::cerr << std::endl;
            usage(argv);
	    return 1;
        }
	    break;
      case 'p':
        try
        {
	        pin = (uint8_t)std::stoi(optarg);
        }
        catch(const std::invalid_argument& e)
        {
	        std::cerr << "Pin must be a number. "<< optarg << "supplied. Aborting." << std::endl;
      	    std::cerr << std::endl;
            usage(argv);
	        return 1;
        }
	    break;
      case 'a':
        device = optarg;
        break;
      case 'r':
        try
        {
            Rvdiv = std::stoi(optarg);
        }
        catch(const std::invalid_argument& e)
        {
            std::cerr << "Resistance must be a number. "<< optarg << "supplied. Aborting." << std::endl;
            std::cerr << std::endl;
            usage(argv);
            return 1;
        }
        break;
      case 'e':
        config_file=optarg;
        break;
      case 'c':
	    try
        {
	        channel=std::stoi(optarg);
        }
	    catch(const std::invalid_argument& e)
        {
	        channelName=optarg;
        }
	    break;
      case 'P':
	    PSname=optarg;
	    break;
      case 'd':
        logIt::incrDebug();
        break;
      case 'h':
        usage(argv);
        return 1;
      default:
        std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
        std::cerr << std::endl;
        usage(argv);
        return 1;
      }
  }
  
  // Get control of the power supply connected to the peltier
  EquipConf hw;
  if(config_file.empty()) {
      std::cerr << "Configuration file not specified. Aborting." << std::endl;
      usage(argv);
      return 1;
  } else {
      hw.setHardwareConfig(config_file);
  }
  
  if(device.empty()) {
      std::cerr << "Arduino port not specified. Aborting." << std::endl;
      usage(argv);
      return 1;
  }
  logger(logDEBUG) << "Device port: " << device;
  // Serial Com to communicate with Arduino connected to NTC
  std::shared_ptr<TextSerialCom> com = std::make_shared<TextSerialCom>(device,B9600);
  com->setTermination("\r\n");
  com->setTimeout(5);
  com->init();
      
  logger(logDEBUG) << "Pin: " << pin;
  
  // Interface to read NTC sensor
  NTCSensor sensor(pin, com, refTemp, Rref, Bntc, Rvdiv, NTCvoltage);
  sensor.init();

  logger(logDEBUG) << "Configuring power supply.";
  if(channelName.empty()) {
      if(PSname.empty()) {
          std::cerr << "Power supply channel not specified. Aborting." << std::endl;
          usage(argv);
          return 1;
      } else {
          std::shared_ptr<IPowerSupply> PSreal = hw.getPowerSupply(PSname);
          PS = std::make_shared<PowerSupplyChannel>(PSname+std::to_string(channel), PSreal, channel);
      }
  } else {
      PS = hw.getPowerSupplyChannel(channelName);
  }
  
  logger(logDEBUG) << "Program power supply.";
  PS->program();

  float v=PS->getVoltageLevel();
  logger(logDEBUG) << "Voltage is: " << PS->getVoltageLevel();
  if(v>max_voltage) {
    logger(logINFO) << "Voltage is too high.";
    return 1;
  }
  if(PS->getVoltageProtect()<max_voltage) {
    logger(logDEBUG)<<" The voltage protect set for this PS is lower than the max voltage allowed for the PID loop";
    return 1;
  }
  logger(logDEBUG) << "Turn on power supply.";
  PS->turnOn();

  // Prepare for PID loop
  Pid TC(&voltage, kp, ki, kd);
  TC.set_output_limits(min_voltage, max_voltage);

  bool temp_reached = false;
  float temp=0;

  // Register signal handler so that appropriate action may be taken 
  // upon keyboard interrupt (Ctrl-C)
  signal(SIGINT, signalHandler);
  
  while (true)
  {
    // Measure temperature using NTC
    try {
      sensor.read();
    } catch(const std::runtime_error& e) {
      logger(logDEBUG) << e.what();
      std::this_thread::sleep_for(std::chrono::seconds(1));
      continue;
    }
    temp=sensor.temperature()+273.15; // in Kelvin
    
    logger(logINFO) << "Temperature: " << temp-273.15 << "C";
    
    // Set power supply to appropriate voltage
    if (abs(set_point - temp) < go_time_temp or temp_reached){
      temp_reached = true;
      if (TC.compute(temp, set_point, duration)) {
        logger(logDEBUG) << "PS Voltage: " << voltage;
        PS->setVoltageLevel(voltage);
      }
    } else if (temp<set_point) {
      PS->setVoltageLevel(min_voltage);
    } else {
      PS->setVoltageLevel(max_voltage);
    }
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }

  return 0;
}



